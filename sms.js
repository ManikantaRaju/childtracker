
var accountSid = 'ACc60875daae89bba4273aaff699a3ee41'; // Your Account SID from www.twilio.com/console
var authToken = '130e bcad4c46edeaf2ed716465ac9b08';   // Your Auth Token from www.twilio.com/console


// Create a new REST API client to make authenticated requests against the
// twilio back end
var twilio = require('twilio');
var client = new twilio(accountSid, authToken);

client.messages.create({
    body: 'Hello from Node',
    to: '+917407461154',  // Text this number
    from: '+12392324567' // From a valid Twilio number
}, function(error, message) {
    if (!error) {
        console.log('Success! The SID for this SMS message is:');
        console.log(message.sid);
        console.log('Message sent on:');
        console.log(message.dateCreated);
    } else {
        console.log('Oops! There was an error.');
    }
});