////account sid and auth token
//var accountSid = 'AC6f94ce907b4b1526347b1bc72b1aa763'; // Your Account SID from www.twilio.com/console
//var authToken = '960e787f5939ff25691cc84c74f47a13'; // Your Auth Token from www.twilio.com/console
//Identifiers for the ids
//editing arabic https://www.lexilogos.com/keyboard/arabic.htm
var TerminalRegisterID = 0x0100;
var TerminalRegisterResponseID = 0x8100;
var TerminalAuthenticationID = 0x0102;
var PlatformGeneralResponseID = 0x8001;
var LocationInformationReport = 0x0200;
var PlatformGeneralResponseID = 0x8001;
var TerminalHeartbeatID = 0x0002;
var TerminalGeneralResponseID = 0x0001;
var RFIDCardReadID = 0x0702;
var BluetoothClientID = 0x0722;
var MessageSerialID = 0;
var DEVICE_ID = [];
var AUTH_KEY = [];
var TS1_LOW = " 20:10:00";
var TS1_HIGH = " 21:57:00";
var TS2_LOW = " 11:02:00";
var TS2_HIGH = " 11:09:00";
var TS3_LOW = " 11:14:00";
var TS3_HIGH = " 11:21:00";
var Students = [];

const StudentState = {
    ON_BUS_TO_SCHOOL: 0,
    OFF_BUS_TO_SCHOOL: 1,
    ON_BUS_TO_HOME: 2,
    OFF_BUS_TO_HOME: 3
}

//var Student = {
//    Time: "",
//    ID: "",
//    State: "",
//    ToNumber: ""
//}; Prototype for student


// Create a new REST API client to make authenticated requests against the
// twilio back end
//var twilio = require('twilio');
//var smsClient = new twilio(accountSid, authToken);
var request = require('request');
//var User = 'manikantaraju'; // Your Account SID from www.twilio.com/console
//var passwd = 'jFfMSFjcVq9cHMT'; // Your Auth Token from www.twilio.com/console
//var ToNumbers = ["96893300022", "96897070465"];
var ToNumbers = {
    '0': "96898077002",
    '1': "96893300011",
    '2': "96893300022",
    '3': "96893225115",
    '4': "96899148563",
    '5': "96892566559",
    '6': "96893311337",
    '7': "96894349841",
    '8': "96898111100"
};

var ToNames = {
    '0': "أحمد",
    '1': "علي",
    '2': "مازن",
    '3': "تركي",
    '4': "ريم",
    '5': "طارق",
    '6': "نوف",
    '7': "جود",
    '8': "سعود"
};
//var toNumber = "96893300022";
var sms_body = "";
//var sms_url = `http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=manikantaraju&passwd=jFfMSFjcVq9cHMT&mobilenumber=96893300022&message=${sms_body}&sid=ALmuznOasis&mtype=OL&DR=N`;
//log.info("Hello World!");
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

//var Student = {
//    Time: "",
//    ID: "",
//    State: "",
//    ToNumber: ""
//}; Prototype for student

// Set some defaults (required if your JSON file is empty)
db.defaults({
        tags: [],
        info: {
            "name": "Students"
        },
        count: 0
    })
    .write();

var net = require("net");
var debug = require('debug')('Debug Log:');
var SimpleNodeLogger = require('simple-node-logger'),
    opts = {
        logFilePath: 'mylogfile.log',
        timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
    },
    log = SimpleNodeLogger.createSimpleFileLogger(opts);
log.info("=======================================================");
log.info("Starting tcp.js");
var server = net.createServer();
server.on("connection", function (socket) {
    var remoteAddress = socket.remoteAddress + ":" + socket.remotePort;
    log.info('REMOTE Socket is listening at port :', socket.remotePort);
    log.info('REMOTE Socket ip :', socket.remoteAddress);

    server.getConnections(function (error, count) {
        log.info('Concurrent connections : ', count);
    });

    socket.on("data", function (d) {
        log.info('Bytes read : ', socket.bytesRead);
        log.info("Req : ", d.toString('hex').match(/../g).join(' '));
        var response = DataParser(d);
        log.info("Res : ", Buffer.from(response).toString('hex').match(/../g).join(' '));
        socket.write(Buffer.from(response));
    });

    socket.on('end', function (data) {
        log.info('Socket ended from other end!');
        log.info('End data : ', data);
    });

    socket.on("close", function (error) {
        log.info('Bytes written : ', socket.bytesWritten);
        log.info("Connection from ", remoteAddress, " closed");
        if (error) {
            log.error('Socket closed due to transmission error');
        }
    });

    socket.on("error", function (err) {
        log.error("Connection error from ", remoteAddress, ' : ', err);
    });

});

server.on("error", function (err) {
    log.error("An error has occurred!");
});

server.maxConnections = 10;

server.listen(9000, function () {
    log.info("server listening to ", server.address());
});

//
function DataParser(rawData) {

    var rawDataLength = rawData.length;

    if (rawDataLength > 0) {
        var startByte = rawData[0];
        var endByte = rawData[rawDataLength - 1];
        if (startByte == 0x7E && endByte == 0x7E) {
            var messageID = (((rawData[1] & 0xFF) << 8) | (rawData[2] & 0xFF)); // decoding Message ID
            var messageBodyLength = (((rawData[3] & 0xFF) << 8) | (rawData[4] & 0xFF)); // decoding the message body length
            var deviceID = rawData.slice(5, 11); // Debive ID
            DEVICE_ID = deviceID;
            var deviceIdPretty = deviceID.toString('hex').match(/../g).join(' ');
            var serialID = (((rawData[11] & 0xFF) << 8) | (rawData[12] & 0xFF)); // Device Serial ID
            //Computing the calibration code
            var checkSumInput = rawData.slice(1, 2 + 2 + 6 + 2 + messageBodyLength);
            var checkSumInputLength = checkSumInput.length;
            var checksum = 0;
            for (var i = 0; i < checkSumInputLength; i++) {
                checksum = checksum ^ checkSumInput[i];
            }

            log.info("Message ID : 0x", rawData.slice(1, 3).toString('hex'));
            log.info("Message Body Length : ", messageBodyLength);
            log.info("Device ID : ", deviceIdPretty);
            log.info("Serial ID : ", serialID.toString(16));

            //conditional processing based on Message IDs
            var serverMessageID = 0;
            var serverMessageLength = 0;
            var serverResponseData = [];

            //1 push start byte 0x7E
            var serverRepsonse = [0x7E]; //Start Byte
            //            log.info("%O", serverRepsonse.toString('hex').match(/../g).join(' '));
            switch (messageID) {
                case TerminalRegisterID:
                    // TODO : Need to register the terminal ID in database
                    //2 push response register id from server 0x8100
                    serverRepsonse.push(0x81);
                    serverRepsonse.push(0x00);

                    log.info("Registering Device with Device ID : ", deviceIdPretty);
                    //                    serverMessageID = 0x8100;
                    //using date as authentication code.

                    serverMessageLength = 14;
                    //3 push message length
                    serverRepsonse.push((serverMessageLength >> 8) & 0xFF);
                    serverRepsonse.push((serverMessageLength) & 0xFF);

                    //4 push sender device ID
                    for (var i = 0; i < deviceID.length; i++) {
                        serverRepsonse.push(deviceID[i]);
                    }

                    //5 Push message serial ID
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //6 Push message id
                    serverRepsonse.push(rawData[1]);
                    serverRepsonse.push(rawData[2]);

                    //7 Push success or failure
                    serverRepsonse.push(0);

                    //8 Push Auth key
                    var key = FormAuthKey();
                    AUTH_KEY = key;
                    for (var i = 0; i < key.length; i++) {

                        serverRepsonse.push(key[i] + 0x30);
                    }
                    var sMessageLength = key.length + 3;
                    serverRepsonse[3] = (sMessageLength >> 8) & 0xFF;
                    serverRepsonse[4] = (sMessageLength) & 0xFF;

                    //9 Calibration Byte
                    var calibrationByte = 0;
                    for (var i = 1; i < serverRepsonse.length; i++) {
                        calibrationByte = calibrationByte ^ serverRepsonse[i];
                    }
                    //10 Push Calibration byte
                    serverRepsonse.push(calibrationByte);

                    //11 Push end byte
                    serverRepsonse.push(0x7E);
                    break;

                case TerminalAuthenticationID:
                    // TODO : Need to register the terminal ID in database
                    //2 push response register id from server 0x8100
                    serverRepsonse.push(0x80);
                    serverRepsonse.push(0x01);

                    log.info("Authenticating Device with Device ID : ", deviceIdPretty);
                    //                    serverMessageID = 0x8100;
                    //using date as authentication code.

                    serverMessageLength = 5;
                    //3 push message length
                    serverRepsonse.push((serverMessageLength >> 8) & 0xFF);
                    serverRepsonse.push((serverMessageLength) & 0xFF);

                    //4 push sender device ID
                    // TODO check if key exust in db and match with auth key
                    for (var i = 0; i < deviceID.length; i++) {
                        serverRepsonse.push(deviceID[i]);
                    }

                    //5 Push server serial ID
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //6 Push tracker serial id
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //7 Push message id
                    serverRepsonse.push(rawData[1]);
                    serverRepsonse.push(rawData[2]);

                    //8 Push success or failure
                    serverRepsonse.push(0);

                    //9 Calibration Byte
                    var calibrationByte = 0;
                    for (var i = 1; i < serverRepsonse.length; i++) {
                        calibrationByte = calibrationByte ^ serverRepsonse[i];
                    }
                    //10 Push Calibration byte
                    serverRepsonse.push(calibrationByte);

                    //11 Push end byte
                    serverRepsonse.push(0x7E);

                    break;

                case LocationInformationReport:
                    // TODO : Need to register the terminal ID in database
                    log.info("Registering location info of Device with Device ID : ", deviceIdPretty);
                    //2 push response register id from server 0x8100
                    serverRepsonse.push(0x80);
                    serverRepsonse.push(0x01);

                    serverMessageLength = 5;
                    //3 push message length
                    serverRepsonse.push((serverMessageLength >> 8) & 0xFF);
                    serverRepsonse.push((serverMessageLength) & 0xFF);

                    //4 push sender device ID
                    // TODO check if key exust in db and match with auth key
                    for (var i = 0; i < deviceID.length; i++) {
                        serverRepsonse.push(deviceID[i]);
                    }

                    //5 Push server serial ID
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //6 Push tracker serial id
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //7 Push message id
                    serverRepsonse.push(rawData[1]);
                    serverRepsonse.push(rawData[2]);

                    //8 Push success or failure
                    serverRepsonse.push(0);

                    //9 Calibration Byte
                    var calibrationByte = 0;
                    for (var i = 1; i < serverRepsonse.length; i++) {
                        calibrationByte = calibrationByte ^ serverRepsonse[i];
                    }
                    //10 Push Calibration byte
                    serverRepsonse.push(calibrationByte);

                    //11 Push end byte
                    serverRepsonse.push(0x7E);

                    break;
                case TerminalHeartbeatID:
                    log.info("Terminal heartbeat");
                    //2 push response register id from server 0x8100
                    serverRepsonse.push(0x80);
                    serverRepsonse.push(0x01);

                    serverMessageLength = 5;
                    //3 push message length
                    serverRepsonse.push((serverMessageLength >> 8) & 0xFF);
                    serverRepsonse.push((serverMessageLength) & 0xFF);

                    //4 push sender device ID
                    // TODO check if key exust in db and match with auth key
                    for (var i = 0; i < deviceID.length; i++) {
                        serverRepsonse.push(deviceID[i]);
                    }

                    //5 Push server serial ID
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //6 Push tracker serial id
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //7 Push message id
                    serverRepsonse.push(rawData[1]);
                    serverRepsonse.push(rawData[2]);

                    //8 Push success or failure
                    serverRepsonse.push(0);

                    //9 Calibration Byte
                    var calibrationByte = 0;
                    for (var i = 1; i < serverRepsonse.length; i++) {
                        calibrationByte = calibrationByte ^ serverRepsonse[i];
                    }
                    //10 Push Calibration byte
                    serverRepsonse.push(calibrationByte);

                    //11 Push end byte
                    serverRepsonse.push(0x7E);
                    break;

                case BluetoothClientID:
                    log.info("Bluetooth client detected");
                    //2 push response register id from server 0x8100
                    serverRepsonse.push(0x80);
                    serverRepsonse.push(0x01);

                    //3 push message length
                    serverMessageLength = 5;
                    serverRepsonse.push((serverMessageLength >> 8) & 0xFF);
                    serverRepsonse.push((serverMessageLength) & 0xFF);

                    //4 push sender device ID
                    // TODO check if key exist in db and match with auth key
                    for (var i = 0; i < deviceID.length; i++) {
                        serverRepsonse.push(deviceID[i]);
                    }

                    //5 Push server serial ID
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //6 Push tracker serial id
                    serverRepsonse.push(rawData[11]);
                    serverRepsonse.push(rawData[12]);

                    //7 Push message id
                    serverRepsonse.push(rawData[1]);
                    serverRepsonse.push(rawData[2]);

                    //8 Push success or failure
                    serverRepsonse.push(0);

                    //9 Calibration Byte
                    var calibrationByte = 0;
                    for (var i = 1; i < serverRepsonse.length; i++) {
                        calibrationByte = calibrationByte ^ serverRepsonse[i];
                    }
                    //10 Push Calibration byte
                    serverRepsonse.push(calibrationByte);

                    //11 Push end byte
                    serverRepsonse.push(0x7E);

                    //Parse data
                    //1. Insert/Remove
                    var retainStatus = rawData[13];
                    var getOnOffStatus = rawData[14];
                    log.info("Get On/Off status : ", getOnOffStatus.toString(16));
                    var pushDate = rawData.slice(15, 18);
                    var pushTime = rawData.slice(18, 21);
                    var dateString = "20" + pushDate.toString('hex').match(/../g).join('-');
                    var timeString = pushTime.toString('hex').match(/../g).join(':');
                    log.info("Insert time : ", dateString, " ", timeString);
                    var dateTime = dateString + ' ' + timeString;
                    retainStatus = rawData[21];
                    var kidIDLength = rawData[22];
                    var kidInfo = rawData.slice(23, 23 + kidIDLength);
                    log.info("Driver ID : ", kidInfo.toString('hex').match(/../g).join(' '));
                    //Send SMS
                    //                    sendSMS(dateTime, kidInfo, cardCount);
                    ValidateStudentID(dateString, timeString, kidInfo.toString('hex').match(/../g).join(' '), deviceID.toString('hex').match(/../g).join(' '), getOnOffStatus)

                    break;

            }
            return serverRepsonse;
        }
    }
}

function FormAuthKey() {
    var d = new Date();
    var yyyy = d.getFullYear();
    var mm = d.getMonth();
    var dd = d.getDate();
    var hh = d.getHours();
    var mimi = d.getMinutes();
    var ss = d.getSeconds();
    var authKey = [];

    PushDigits(yyyy, 4, authKey);
    PushDigits(mm, 2, authKey);
    PushDigits(dd, 2, authKey);
    PushDigits(hh, 2, authKey);
    PushDigits(mimi, 2, authKey);
    PushDigits(ss, 2, authKey);
    return authKey;
}

function PushDigits(num, digits, arr) {
    while (digits > 0) {
        arr.push((num % 10));
        num = Math.floor(num / 10);
        digits--;
    }
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear() + ' ' + hours + ':' + minutes + ' ' + ampm;
    //    var strTime = ampm + ' ' + hours + ':' + minutes + ' ' + date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
    return strTime;
}

function sendSMS(dateTime, driverID, state) {

    var id = parseInt(driverID.substring(30, 32), 16);
    id = String.fromCharCode(id);
    var toNumber = ToNumbers[id];
    var iid = ToNames[id];
    var salutation = "عزيزي ولي الأمر";
    var mm0 = " الى المدرسة";
    var ss00 = "قد ركب الحافلة في";
    var ss01 = "ابنكم الطالب ";
    var ss10 = "قد وصل للمدرسة بسلام في";
    var ss11 = "ابنكم الطالب ";
    var ss20 = "قد وصل للمنزل بسلام في";
    var ss21 = "ابنكم الطالب ";
    if (state % 3 == 0)
        sms_body = salutation + "\n" + ss01 + iid + "\n" + ss00 + " (" + dateTime + ") " + "متجهاً" + "\n" + mm0;
    else if (state % 3 == 1)
        sms_body = salutation + "\n" + ss11 + iid + "\n" + ss10 + " (" + dateTime + ") ";
    else
        sms_body = salutation + "\n" + ss21 + iid + "\n" + ss20 + " (" + dateTime + ") ";

    log.info("Sending message to ", toNumber, " Message body \n", sms_body);
    sms_body = encodeURIComponent(sms_body);
    request(sms_url(sms_body, toNumber), function (error, response, body) {
        log.info(body)
        if (!error && response.statusCode == 200) {
            log.info('SMS sent successfully!', sms_url(sms_body, toNumber));
            log.info('Message sent on : ', dateTime);
        } else {
            log.error('Error in sending SMS ', sms_body);
        }
    });
}

function sms_url(message_body, mobile_number) {
    return `http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=manikantaraju&passwd=jFfMSFjcVq9cHMT&mobilenumber=${mobile_number}&message=${message_body}&sid=ALmuznOasis&mtype=LNG&DR=N`;
}

function ValidateStudentID(dateString, timeString, kidID, deviceID, onOffStatus) {

    var dateTime = dateString + ' ' + timeString;
    var formattedDate = formatAMPM(new Date(dateTime));
    var state;
    if (onOffStatus == 255) {
        state = 0;
    } else {
        state = 1;
    }

    var s = db.get('tags')
        .find({
            Id: kidID
        })
        .value();

    //1. Check if student exists in the array
    if (!s) {
        //student doesnt exist
        //push the new student and send sms
        //that the student boarded school bus
        if (state == 0) {
            db.get('tags')
                .push({
                    Time: dateTime,
                    Id: kidID,
                    DeviceId: deviceID,
                    LastState: StudentState.ON_BUS_TO_SCHOOL
                })
                .write();
            // Increment count
            db.update('count', n => n + 1)
                .write();
            sendSMS(formattedDate, kidID, 0);
        }


    } else {

        switch (s.LastState) {

            case StudentState.ON_BUS_TO_SCHOOL:
                if (state == 1) {
                    db.get('tags')
                        .find({
                            Id: kidID
                        }).assign({
                            LastState: StudentState.OFF_BUS_TO_SCHOOL
                        })
                        .write();
                    sendSMS(formattedDate, kidID, 1);
                }
                break;
            case StudentState.OFF_BUS_TO_SCHOOL:
                if (state == 0) {
                    db.get('tags')
                        .find({
                            Id: kidID
                        }).assign({
                            LastState: StudentState.ON_BUS_TO_HOME
                        })
                        .write();
                }
                break;
            case StudentState.ON_BUS_TO_HOME:
                if (state == 1) {
                    db.get('tags')
                        .remove({
                            Id: kidID
                        })
                        .write();
                    db.update('count', n => n - 1)
                        .write();
                    sendSMS(formattedDate, kidID, 2);
                }
                break;
        }
    }
}
